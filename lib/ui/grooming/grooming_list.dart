import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:get_it/get_it.dart';
import 'package:happypaws/helpers/format_helper.dart';
import 'package:happypaws/models/attendant_model.dart';
import 'package:happypaws/models/branch_model.dart';
import 'package:happypaws/models/grooming_model.dart';
import 'package:happypaws/models/grooming_service_model.dart';
import 'package:happypaws/stores/grooming_store.dart';
import 'package:happypaws/widgets/elevated_button_widget.dart';
import 'package:happypaws/widgets/link_button_widget.dart';
import 'package:happypaws/widgets/table_header_widget.dart';
import 'package:happypaws/widgets/table_row_widget.dart';
import 'package:happypaws/widgets/text_widget.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:toggle_switch/toggle_switch.dart';

class GroomingList extends StatefulWidget {
  const GroomingList({Key? key}) : super(key: key);

  @override
  State<GroomingList> createState() => _GroomingListState();
}

class _GroomingListState extends State<GroomingList> {
  late GroomingStore _groomingStore;

  List<GroomingService> services = [];
  List<Attendant> attendants = [];

  bool reload = false;

  DateFormat dateFormat = DateFormat('MMM dd, yyyy HH:mm');

  DateFormat dateFilterFormat = DateFormat('MMM dd, yyyy');

  double total = 0;

  int selectedAttendant = 0;

  String filteredAttendant = "All";
  String filteredService = "All";

  DateTime filteredFromDate = DateTime.now();
  DateTime filteredToDate = DateTime.now();

  @override
  void initState() {
    _groomingStore = GetIt.I<
        GroomingStore>(); //Provider.of<GroomingStore>(context, listen: false);

    getGroomingMasterList();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Container(
            padding: const EdgeInsets.all(8),
            decoration: const BoxDecoration(
                border: Border(
                    bottom: BorderSide(
              color: Colors.grey,
              width: 1.0,
            ))),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                    padding: const EdgeInsets.fromLTRB(15, 10, 10, 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        Text(
                          'Happy',
                          style: TextStyle(fontSize: 30, color: Colors.blue),
                        ),
                        Text(
                          'Paws',
                          style: TextStyle(fontSize: 30, color: Colors.pink),
                        ),
                      ],
                    )),
                Row(
                  children: [
                    _datePicker("From", filteredFromDate, (date) {
                      setState(() {
                        reload = true;
                        filteredFromDate = date;
                      });
                    }),
                    const SizedBox(width: 10),
                    _datePicker("To", filteredToDate, (date) {
                      setState(() {
                        reload = true;
                        filteredToDate = date;
                      });
                    }),
                    const SizedBox(width: 10),
                    _filterAttendants(),
                    const SizedBox(width: 10),
                    _filterServices(),
                    const SizedBox(width: 10),
                    SizedBox(
                      width: 100,
                      height: 48,
                      child: ElevatedButtonWidget(
                        'Refresh',
                        () {
                          setState(() {
                            filteredAttendant = "All";
                            filteredService = "All";
                            reload = true;
                          });
                        },
                        color: Colors.blueAccent,
                        size: 18,
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
          Expanded(child: SingleChildScrollView(child: _list())),
        ],
      ),
    );
  }

  Widget _datePicker(
      String label, DateTime date, Function(DateTime) onPressed) {
    return TextButton(
        onPressed: () {
          DatePicker.showDatePicker(context,
              showTitleActions: true,
              minTime: DateTime(2022, 1, 1),
              maxTime: DateTime.now(), //DateTime(2019, 6, 7),
              theme: const DatePickerTheme(
                  headerColor: Colors.blue,
                  backgroundColor: Colors.white,
                  itemStyle: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 16),
                  doneStyle: TextStyle(color: Colors.white, fontSize: 16)),
              onChanged: (date) {},
              onConfirm: onPressed,
              currentTime: date,
              locale: LocaleType.en);
        },
        child: Column(
          children: [
            TextWidget(
              label,
              color: Colors.grey,
            ),
            Text(
              dateFilterFormat.format(date),
              style: const TextStyle(fontSize: 16, color: Colors.blue),
            ),
          ],
        ));
  }

  Widget _filterAttendants() {
    if (attendants.isEmpty) return Container();

    List<DropdownMenuItem<String>> items = [
      const DropdownMenuItem(
          value: 'All',
          child: TextWidget(
            'All - Attendants',
            color: Colors.pink,
            size: 16,
          )),
      ...attendants
          .map((e) => DropdownMenuItem(
              value: e.id,
              child: TextWidget(
                e.name,
                color: Colors.pink,
                size: 16,
              )))
          .toList()
    ];

    return Container(
      padding: const EdgeInsets.only(left: 20, right: 10),
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(20)),
      child: DropdownButton(
          borderRadius: BorderRadius.circular(20),
          dropdownColor: Colors.white,
          value: filteredAttendant,
          items: items,
          onChanged: (value) {
            setState(() {
              filteredAttendant = value as String;
            });
          }),
    );
  }

  Widget _filterServices() {
    if (services.isEmpty) return Container();

    List<DropdownMenuItem<String>> items = [
      const DropdownMenuItem(
          value: 'All',
          child: TextWidget(
            'All - Services',
            color: Colors.green,
            size: 16,
          )),
      ...services
          .map((e) => DropdownMenuItem(
              value: e.id,
              child: TextWidget(
                e.name,
                color: Colors.green,
                size: 16,
              )))
          .toList()
    ];

    return Container(
      padding: const EdgeInsets.only(left: 20, right: 10),
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(20)),
      child: DropdownButton(
          borderRadius: BorderRadius.circular(20),
          dropdownColor: Colors.white,
          value: filteredService,
          items: items,
          onChanged: (value) {
            setState(() {
              filteredService = value as String;
            });
          }),
    );
  }

  Widget _list() {
    return FutureBuilder(
        future: _groomingStore.getGroomings(
            reload, filteredFromDate, filteredToDate,
            attendant: filteredAttendant, service: filteredService),
        builder:
            (BuildContext context, AsyncSnapshot<List<Grooming>> snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              reload = false;
              return const Padding(
                padding: EdgeInsets.all(20),
                child: TextWidget(
                  'Loading... Please wait.',
                  size: 18,
                  color: Colors.black,
                ),
              );
            case ConnectionState.done:
              if (snapshot.data == null) {
                return const Text(
                    'Oops! An error occurred while fetching data.');
              }

              total = 0;
              if (snapshot.data!.isNotEmpty) {
                var fees = snapshot.data!.map((e) => e.service?.rate ?? 0);
                total = fees.reduce((value, element) => value + element);
              }

              return _table(snapshot.data ?? []);
            default:
              if (snapshot.hasError) {
                return Text('Error: ${snapshot.error}');
              } else {
                return Container();
              }
          }
        });
  }

  Widget _table(List<Grooming> data) {
    return Container(
      color: Colors.white,
      padding: const EdgeInsets.all(20.0),
      child: Table(
        border: TableBorder.all(color: Colors.black54),
        children: [
          const TableRow(children: [
            TableHeaderWidget('Date'),
            TableHeaderWidget('Branch'),
            TableHeaderWidget('Owner'),
            TableHeaderWidget('Pet'),
            TableHeaderWidget('Attendant'),
            TableHeaderWidget('Service'),
            TableHeaderWidget('Fee'),
          ]),
          ..._buildList(data),
          TableRow(children: [
            const TableHeaderWidget(''),
            const TableHeaderWidget(''),
            const TableHeaderWidget(''),
            const TableHeaderWidget(''),
            const TableHeaderWidget(''),
            const TableHeaderWidget(''),
            TableHeaderWidget(FormatHelper.currency(total)),
          ]),
        ],
      ),
    );
  }

  List<TableRow> _buildList(List<Grooming> data) {
    List<TableRow> rows = [];

    for (var element in data) {
      rows.add(TableRow(children: [
        TableRowWidget(
          dateFormat.format(element.createdAt),
          textAlign: TextAlign.center,
        ),
        TableRowWidget(Branch.getBranchName(element.branch)),
        _ownerRow(element),
        _petRow(element),
        _attendantRowButton(element),
        _serviceRowButton(element),
        TableRowWidget(
          FormatHelper.currency(element.service?.rate ?? 0),
          textAlign: TextAlign.center,
        ),
      ]));
    }

    return rows;
  }

  Widget _attendantRowButton(Grooming grooming) {
    return SizedBox(
      width: 100,
      child: Padding(
        padding: const EdgeInsets.all(5),
        child: LinkButtonWidget(
          grooming.attendant == null ? 'Attendant' : grooming.attendant!.name,
          () {
            attendantModal(grooming);
          },
          color: grooming.attendant != null ? Colors.pink : Colors.blue,
        ),
      ),
    );
  }

  Widget _serviceRowButton(Grooming grooming) {
    return SizedBox(
      width: 100,
      child: Padding(
        padding: const EdgeInsets.all(5),
        child: LinkButtonWidget(
          grooming.service == null ? 'Service' : grooming.service!.name,
          () {
            groomingServicesModal(grooming);
          },
          color: grooming.service != null ? Colors.green : Colors.blue,
        ),
      ),
    );
  }

  void groomingServicesModal(Grooming grooming) {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
            Container(
              width: double.infinity,
              color: Colors.green,
              child: const Padding(
                padding: EdgeInsets.all(8.0),
                child: Center(
                    child: TextWidget('Select Service',
                        size: 18, color: Colors.white)),
              ),
            ),
            Expanded(
                flex: 1,
                child: SingleChildScrollView(
                  child: Column(
                    children: [..._services(grooming)],
                  ),
                ))
          ]);
        });
  }

  List<Widget> _services(Grooming grooming) {
    if (services.isEmpty) return [];

    List<Widget> widgets = [];

    for (var service in services) {
      widgets.add(ListTile(
        //leading: new Icon(Icons.photo),
        title: TextWidget(service.name),
        trailing: TextWidget(FormatHelper.currency(service.rate)),
        onTap: () {
          setState(() {
            grooming.service = service;
          });

          _groomingStore
              .updateGrooming({'id': grooming.id, 'service': service.id});

          Navigator.pop(context);
        },
      ));
    }

    return widgets;
  }

  void attendantModal(Grooming grooming) {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
            Container(
              width: double.infinity,
              color: Colors.pink,
              child: const Padding(
                padding: EdgeInsets.all(8.0),
                child: Center(
                    child: TextWidget("Select Attendant",
                        size: 18, color: Colors.white)),
              ),
            ),
            Expanded(
              flex: 1,
              child: SingleChildScrollView(
                child: Column(
                  children: [..._attendants(grooming)],
                ),
              ),
            )
          ]);
        });
  }

  List<Widget> _attendants(Grooming grooming) {
    if (attendants.isEmpty) return [];

    List<Widget> widgets = [];

    for (var attendant in attendants) {
      widgets.add(ListTile(
        title: TextWidget(attendant.name),
        onTap: () {
          setState(() {
            grooming.attendant = attendant;
          });

          _groomingStore.updateGrooming({
            'id': grooming.id,
            'attendant': attendant.id,
          });

          Navigator.pop(context);
        },
      ));
    }

    return widgets;
  }

  void getGroomingMasterList() async {
    final groomingServices = await _groomingStore.getGroomingServices();
    final groomingAttendants = await _groomingStore.getGroomingAttendants();

    setState(() {
      services = groomingServices;
      attendants = groomingAttendants;
    });
  }

  Widget _ownerRow(Grooming grooming) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              const Icon(
                Icons.person,
                color: Colors.grey,
                size: 20,
              ),
              const SizedBox(width: 5),
              Flexible(
                child: TextWidget(
                  grooming.owner,
                ),
              ),
            ],
          ),
          const SizedBox(height: 15),
          Row(
            children: [
              const Icon(
                Icons.phone_android,
                color: Colors.grey,
                size: 20,
              ),
              const SizedBox(width: 5),
              TextWidget(grooming.contact),
            ],
          ),
          const SizedBox(height: 15),
          Row(
            children: [
              const Icon(
                Icons.pin_drop,
                color: Colors.grey,
                size: 20,
              ),
              const SizedBox(width: 5),
              Flexible(child: TextWidget(grooming.address)),
            ],
          ),
        ],
      ),
    );
  }

  Widget _petRow(Grooming grooming) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              const Icon(
                Icons.pets,
                color: Colors.grey,
                size: 20,
              ),
              const SizedBox(width: 5),
              TextWidget(
                grooming.pet,
              ),
            ],
          ),
          const SizedBox(height: 15),
          TextWidget('Breed: ' + grooming.breed),
          const SizedBox(height: 15),
          TextWidget('Age: ' + grooming.age),
        ],
      ),
    );
  }
}
