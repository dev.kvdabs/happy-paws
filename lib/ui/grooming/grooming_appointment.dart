// ignore_for_file: unnecessary_new

import 'dart:html';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:happypaws/stores/grooming_store.dart';
import 'package:happypaws/widgets/elevated_button_widget.dart';
import 'package:happypaws/widgets/label_widget.dart';
import 'package:happypaws/widgets/phone_number_field.dart';
import 'package:happypaws/widgets/text_widget.dart';
import 'package:happypaws/widgets/textfield_widget.dart';
import 'package:provider/provider.dart';

class GroomingAppointment extends StatefulWidget {
  final String branch;
  const GroomingAppointment(this.branch, {Key? key}) : super(key: key);

  @override
  State<GroomingAppointment> createState() => _GroomingAppointmentState();
}

class _GroomingAppointmentState extends State<GroomingAppointment>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;

  final _formKey = GlobalKey<FormState>();

  final TextEditingController _ownerController = new TextEditingController();
  final TextEditingController _contactController = new TextEditingController();
  final TextEditingController _petController = new TextEditingController();
  final TextEditingController _breedController = new TextEditingController();
  final TextEditingController _addressController = new TextEditingController();
  final TextEditingController _ageController = new TextEditingController();

  late String ownerName;
  late String contact;
  late String petName;
  late String breed;
  late String address;
  String? age;

  String screenView = "entry";

  late GroomingStore _groomingStore;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(vsync: this);
    _groomingStore = GetIt.I<
        GroomingStore>(); //Provider.of<GroomingStore>(context, listen: false);
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          decoration: const BoxDecoration(
              image: DecorationImage(
            image: AssetImage("images/background.jpeg"),
            fit: BoxFit.cover,
          )),
          child: new BackdropFilter(
            filter: new ImageFilter.blur(sigmaX: 8.0, sigmaY: 8.0),
            child: Container(
              decoration:
                  new BoxDecoration(color: Colors.white.withOpacity(0.1)),
              child: ListView(
                children: [_header(), _body(), _footer()],
              ),
            ),
          )),
    );
  }

  Widget _header() {
    return Container(
        width: double.infinity,
        //color: Colors.grey,
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Column(
          //mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              "images/logo.png",
              width: 150,
            ),
            const SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: const [
                Text(
                  'Happy',
                  style: TextStyle(fontSize: 24, color: Colors.blue),
                ),
                Text(
                  'Paws',
                  style: TextStyle(fontSize: 24, color: Colors.pink),
                ),
              ],
            )
          ],
        ));
  }

  Widget _body() {
    return Column(children: [
      const SizedBox(
        height: 20,
      ),
      Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          //mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[_screen()],
        ),
      ),
    ]);
  }

  Widget _screen() {
    if (screenView == "confirm") return _confirm();
    if (screenView == "saving") return _saving();
    if (screenView == "submitted") return _submitted();

    return _form();
  }

  Widget _form() {
    return Form(
        key: _formKey,
        child: Column(
          children: [
            TextFieldWidget('Owner\'s Name', _ownerController,
                validator: (String? value) {
              if (value == null || value.isEmpty) {
                return 'Please enter Owner\'s Name';
              }
              return null;
            }),
            const SizedBox(height: 20),
            PhoneNumberFieldWidget('Contact', _contactController,
                validator: (String? value) {
              if (value == null || value.isEmpty) {
                return 'Please enter Contact';
              }
              return null;
            }),
            const SizedBox(height: 20),
            TextFieldWidget('Address', _addressController,
                validator: (String? value) {
              if (value == null || value.isEmpty) {
                return 'Please enter Address';
              }
              return null;
            }),
            const SizedBox(height: 20),
            TextFieldWidget('Pet\'s Name', _petController,
                validator: (String? value) {
              if (value == null || value.isEmpty) {
                return 'Please enter Pet\'s Name';
              }
              return null;
            }),
            const SizedBox(height: 20),
            TextFieldWidget('Breed', _breedController,
                validator: (String? value) {
              if (value == null || value.isEmpty) {
                return 'Please enter Breed';
              }
              return null;
            }),
            const SizedBox(height: 20),
            PhoneNumberFieldWidget('Age', _ageController),
          ],
        ));
  }

  Widget _footer() {
    Widget _button = Container();
    if (screenView == "entry") {
      _button = _continueButton();
    } else if (screenView == "confirm") {
      _button = _submitButton();
    }

    return Padding(
      padding: const EdgeInsets.all(20),
      child: SizedBox(
          //width: MediaQuery.of(context).size.width / 2,
          height: 50,
          child: _button),
    );
  }

  Widget _submitButton() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        ElevatedButtonWidget(
          'Back',
          () {
            setState(() {
              screenView = "entry";
            });
          },
          color: Colors.grey,
          size: 24,
        ),
        ElevatedButtonWidget(
          "Submit",
          () {
            // save
            setState(() {
              screenView = "saving";
            });
          },
          size: 24,
        )
      ],
    );
  }

  Widget _continueButton() {
    return ElevatedButtonWidget(
      "Continue",
      () {
        if (_formKey.currentState!.validate()) {
          setState(() {
            screenView = "confirm";
          });
        }
      },
      size: 24,
    );
  }

  Widget _confirm() {
    return Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          children: [
            LabelWidget('Owner', _ownerController.text),
            LabelWidget('Contact', _contactController.text),
            LabelWidget('Pet\'s Name', _petController.text),
            LabelWidget('Breed', _breedController.text),
            LabelWidget('Address', _addressController.text),
            LabelWidget('Age', _ageController.text),
          ],
        ));
  }

  Widget _saving() {
    return FutureBuilder<bool>(
      future: save(), // async work
      builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
            return const TextWidget(
              'Saving... Please wait.',
              size: 18,
              color: Colors.black,
            );
          case ConnectionState.done:
            if (snapshot.data ?? false) {
              screenView = "submitted";
              return _submitted();
            }

            return Container();
          default:
            if (snapshot.hasError) {
              return Text('Error: ${snapshot.error}');
            } else {
              return Container();
            }
        }
      },
    );
  }

  Future<bool> save() async {
    var data = {
      "branch": widget.branch,
      "owner": _ownerController.text,
      "contact": _contactController.text,
      "address": _addressController.text,
      "pet": _petController.text,
      "breed": _breedController.text,
      "age": _ageController.text
    };

    return await _groomingStore.addGrooming(data);
  }

  Widget _submitted() {
    return Center(
      child: Column(
        children: const [
          Icon(
            Icons.check_circle_outline,
            size: 40,
            color: Colors.green,
          ),
          SizedBox(
            height: 10,
          ),
          TextWidget("Submitted. Please proceed to cashier", size: 18)
        ],
      ),
    );
  }
}
