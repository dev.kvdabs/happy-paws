import 'package:flutter/foundation.dart';

class LogHelper {
  static void log(dynamic log) {
    if (kDebugMode) print(log);
  }
}
