import 'package:money_formatter/money_formatter.dart';

class FormatHelper {
  static String currency(double amount) {
    MoneyFormatter rate = MoneyFormatter(amount: amount);
    return rate.output.nonSymbol;
  }
}
