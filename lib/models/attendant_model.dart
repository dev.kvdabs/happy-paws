import 'package:json_annotation/json_annotation.dart';

part 'attendant_model.g.dart';

@JsonSerializable()
class Attendant {
  String id;
  String name;

  Attendant(this.id, this.name);

  factory Attendant.fromJson(Map<String, dynamic> json) =>
      _$AttendantFromJson(json);
}
