part of 'grooming_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

//import 'package:happypaws/models/grooming_model.dart';

Grooming _$GroomingFromJson(Map<String, dynamic> json) {
  return Grooming(
    json['_id'] as String,
    json['branch'] as String,
    json['owner'] as String,
    json['contact'] as String,
    json['address'] as String,
    json['pet'] as String,
    json['breed'] as String,
    json['age'] as String,
    json['_o_attendant'] == null
        ? null
        : Attendant.fromJson(json['_o_attendant']),
    json['_o_service'] == null
        ? null
        : GroomingService.fromJson(json['_o_service']),
    json['rate'] != null ? json['rate'] as double : null,
    DateTime.fromMillisecondsSinceEpoch(json['_createdAt']),
  );
}
