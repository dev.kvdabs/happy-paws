import 'package:json_annotation/json_annotation.dart';

part 'grooming_service_model.g.dart';

@JsonSerializable()
class GroomingService {
  String id;
  String name;
  double rate;

  GroomingService(this.id, this.name, this.rate);

  factory GroomingService.fromJson(Map<String, dynamic> json) =>
      _$GroomingServiceFromJson(json);
}
