import 'package:happypaws/models/attendant_model.dart';
import 'package:happypaws/models/grooming_service_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'grooming_model.g.dart';

@JsonSerializable()
class Grooming {
  String id;
  String branch;
  String owner;
  String contact;
  String address;
  String pet;
  String breed;
  String age;
  Attendant? attendant;
  GroomingService? service;
  double? rate;
  DateTime createdAt;

  Grooming(
      this.id,
      this.branch,
      this.owner,
      this.contact,
      this.address,
      this.pet,
      this.breed,
      this.age,
      this.attendant,
      this.service,
      this.rate,
      this.createdAt);

  factory Grooming.fromJson(Map<String, dynamic> json) =>
      _$GroomingFromJson(json);
}
