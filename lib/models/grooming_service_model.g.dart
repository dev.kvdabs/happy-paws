part of 'grooming_service_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GroomingService _$GroomingServiceFromJson(Map<String, dynamic> json) {
  return GroomingService(
      json['_id'] as String, json['name'] as String, json['rate'] as double);
}
