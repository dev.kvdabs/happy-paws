List<Branch> branches = [
  Branch('001', 'Banilad', '123 Street Mabolo, Cebu City'),
  Branch('002', 'VRama', '123 Street Capitol, Cebu City'),
  Branch('003', 'Mactan', '123 Street Lahug, Cebu City')
];

class Branch {
  String id;
  String name;
  String address;

  Branch(this.id, this.name, this.address);

  static String getBranchName(String id) {
    try {
      final branch = branches.firstWhere((element) => element.id == id);

      return branch.name;
    } catch (e) {
      return "Banilad";
    }
  }
}
