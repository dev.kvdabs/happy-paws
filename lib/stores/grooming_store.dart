import 'dart:convert';

import 'package:get_it/get_it.dart';
import 'package:happypaws/api/repository.dart';
import 'package:happypaws/models/attendant_model.dart';
import 'package:happypaws/models/grooming_model.dart';
import 'package:happypaws/models/grooming_service_model.dart';

class GroomingStore {
  final _repository = GetIt.instance<Repository>();

  List<Grooming> groomings = [];

  Future<List<Grooming>> getGroomings(
      bool reload, DateTime dateFrom, DateTime dateTo,
      {String attendant = "All", String service = "All"}) async {
    if (groomings.isNotEmpty) {
      var filtered = <Grooming>[];

      if (!reload) {
        filtered = groomings
            .where((element) =>
                element.attendant?.id ==
                    (attendant == "All" ? element.attendant?.id : attendant) &&
                element.service?.id ==
                    (service == "All" ? element.service?.id : service))
            .toList();

        return filtered;
      }
    }

    var filteredDateFrom =
        DateTime(dateFrom.year, dateFrom.month, dateFrom.day, 0, 0, 0)
            .millisecondsSinceEpoch;
    var filteredDateTo =
        DateTime(dateTo.year, dateTo.month, dateTo.day, 23, 59, 59)
            .millisecondsSinceEpoch;

    groomings =
        await _repository.getGroomings(filteredDateFrom, filteredDateTo);

    return groomings;
  }

  Future<bool> addGrooming(dynamic data) async {
    return await _repository.addGrooming(data);
  }

  Future<List<GroomingService>> getGroomingServices() async {
    return await _repository.getGroomingServices();
  }

  Future<bool> updateGrooming(Map<String, dynamic> updates) async {
    return await _repository.updateGrooming(updates);
  }

  Future<List<Attendant>> getGroomingAttendants() async {
    return await _repository.getGroomingAttendants();
  }
}
