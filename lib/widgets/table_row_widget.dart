import 'package:flutter/material.dart';

class TableRowWidget extends StatelessWidget {
  final String label;
  final TextAlign textAlign;
  const TableRowWidget(this.label, {Key? key, this.textAlign = TextAlign.left})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Text(label,
          textAlign: textAlign,
          style: const TextStyle(fontWeight: FontWeight.normal)),
    );
  }
}
