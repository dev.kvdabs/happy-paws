import 'package:flutter/material.dart';

class TableHeaderWidget extends StatelessWidget {
  final String label;
  final double size;
  const TableHeaderWidget(this.label, {Key? key, this.size = 16})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey[300],
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Text(label,
            textAlign: TextAlign.center,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: size)),
      ),
    );
  }
}
