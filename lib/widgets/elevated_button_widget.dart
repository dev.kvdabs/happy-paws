// ignore_for_file: unnecessary_new

import 'package:flutter/material.dart';

class ElevatedButtonWidget extends StatelessWidget {
  final String text;
  final Color color;
  final void Function() onPressed;
  final double size;
  const ElevatedButtonWidget(this.text, this.onPressed,
      {Key? key, this.color = Colors.blue, this.size = 14})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        child: Text(
          text,
          style: TextStyle(fontSize: size),
        ),
        onPressed: onPressed,
        style: ElevatedButton.styleFrom(
          //padding: const EdgeInsets.all(10),
          primary: color,
          shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(30.0),
          ),
        ));
  }
}
