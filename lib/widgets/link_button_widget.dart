import 'package:flutter/material.dart';

class LinkButtonWidget extends StatelessWidget {
  final void Function() onPressed;
  final String text;
  final Color color;
  const LinkButtonWidget(this.text, this.onPressed,
      {Key? key, this.color = Colors.blue})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
        onPressed: onPressed,
        child: Text(text, style: TextStyle(color: color)));
  }
}
