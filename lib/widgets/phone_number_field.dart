import 'package:flutter/material.dart';

class PhoneNumberFieldWidget extends StatelessWidget {
  final String label;
  final String? Function(String?)? validator;
  //final void Function(String?) onChange;
  final TextEditingController controller;
  const PhoneNumberFieldWidget(this.label, this.controller,
      {Key? key, this.validator})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      keyboardType: TextInputType.phone,
      validator: validator,
      controller: controller,
      style: const TextStyle(fontSize: 18),
      decoration: InputDecoration(
          fillColor: Colors.white.withOpacity(0.3),
          filled: true,
          contentPadding: const EdgeInsets.fromLTRB(25, 25, 20, 25),
          labelText: label,
          floatingLabelStyle: const TextStyle(fontSize: 24, color: Colors.blue),
          border: OutlineInputBorder(
              borderSide: BorderSide.none,
              borderRadius: BorderRadius.circular(30))),
    );
  }
}
