import 'package:flutter/material.dart';

class LabelWidget extends StatelessWidget {
  final String label;
  final String value;
  const LabelWidget(this.label, this.value, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(label, style: const TextStyle(fontSize: 16, color: Colors.grey)),
        const SizedBox(height: 5),
        Text(value,
            style: const TextStyle(
                fontSize: 20,
                color: Colors.black,
                fontWeight: FontWeight.bold)),
        const SizedBox(height: 15),
      ],
    );
  }
}
