import 'package:get_it/get_it.dart';
import 'package:happypaws/api/grooming_api.dart';
import 'package:happypaws/api/repository.dart';
import 'package:happypaws/stores/grooming_store.dart';

void setup() {
  final getIt = GetIt.instance;

  getIt.registerLazySingleton<Repository>(() => Repository());
  getIt.registerLazySingleton<GroomingApi>(() => GroomingApi());
  getIt.registerLazySingleton<GroomingStore>(() => GroomingStore());
}
