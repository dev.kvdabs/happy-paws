import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:happypaws/api/repository.dart';
import 'package:happypaws/routes.dart';
import 'package:happypaws/service_locator.dart';
import 'package:happypaws/stores/grooming_store.dart';
import 'package:happypaws/ui/grooming/grooming_appointment.dart';
import 'package:happypaws/ui/grooming/grooming_list.dart';
import 'package:provider/provider.dart';

void main() {
  setup();

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Happy Paws',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      //routes: Routes.routes,
      initialRoute: '/',
      onGenerateRoute: generateRoute,
      home: const GroomingAppointment('001'), //const GroomingList() //
    );
  }

  Route<dynamic> generateRoute(RouteSettings settings) {
    var uri = Uri.parse(settings.name ?? '/');
    final path = uri.path.split('/')[2];
    final branch = uri.path.split('/').last;

    switch (path) {
      case 'list':
        return MaterialPageRoute(builder: (_) => const GroomingList());
      case 'branch':
        return MaterialPageRoute(builder: (_) => GroomingAppointment(branch));
      default:
        return MaterialPageRoute(
            builder: (_) => const GroomingAppointment('001'));
    }
  }
}
