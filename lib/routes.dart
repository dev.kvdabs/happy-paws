import 'package:flutter/material.dart';
import 'package:happypaws/ui/grooming/grooming_appointment.dart';
import 'package:happypaws/ui/grooming/grooming_list.dart';

class Routes {
  Routes._();

  //static variables
  static const String grooming = '/grooming/:branch';
  static const String groomingList = '/grooming/list';

  static final routes = <String, WidgetBuilder>{
    // grooming: (BuildContext context) => const GroomingAppointment(),
    groomingList: (BuildContext context) => const GroomingList(),
  };
}
