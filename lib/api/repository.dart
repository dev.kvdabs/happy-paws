import 'package:get_it/get_it.dart';
import 'package:happypaws/api/grooming_api.dart';
import 'package:happypaws/models/attendant_model.dart';
import 'package:happypaws/models/grooming_model.dart';
import 'package:happypaws/models/grooming_service_model.dart';

class Repository {
  final _groomingApi = GetIt.instance<GroomingApi>();

  Repository();

  Future<dynamic> getGroomings(int dateFrom, int dateTo,
      {String branch = ''}) async {
    return await _groomingApi.getGroomings(branch, dateFrom, dateTo);
  }

  Future<bool> addGrooming(dynamic data) async {
    return await _groomingApi.addGrooming(data);
  }

  Future<List<GroomingService>> getGroomingServices() async {
    return await _groomingApi.getGroomingServices();
  }

  Future<bool> updateGrooming(Map<String, dynamic> updates) async {
    return await _groomingApi.updateGrooming(updates);
  }

  Future<List<Attendant>> getGroomingAttendants() async {
    return await _groomingApi.getGroomingAttendants();
  }
}
