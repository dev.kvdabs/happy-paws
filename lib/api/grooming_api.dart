import 'dart:convert';

import 'package:happypaws/helpers/log_helper.dart';
import 'package:happypaws/models/attendant_model.dart';
import 'package:happypaws/models/grooming_model.dart';
import 'package:happypaws/models/grooming_service_model.dart';

import 'package:http/http.dart' as http;

class GroomingApi {
  String base =
      'https://happypaws-347709.as.r.appspot.com/grooming'; // 'http://localhost:3000/grooming'; //

  Future<List<Grooming>> getGroomings(
      String branch, int dateFrom, int dateTo) async {
    final res = await http
        .get(Uri.parse('$base/list?dateFrom=$dateFrom&dateTo=$dateTo'));

    var data = <Grooming>[];

    try {
      for (var item in (jsonDecode(res.body) as List<dynamic>)) {
        data.add(Grooming.fromJson(item as Map<String, dynamic>));
      }
    } catch (err) {
      LogHelper.log(err);
    }

    return data;
  }

  Future<bool> addGrooming(params) async {
    try {
      await http.post(Uri.parse('$base/add'), body: params);
      return true;
    } catch (err) {
      return false;
    }
  }

  Future<List<GroomingService>> getGroomingServices() async {
    final res = await http.get(Uri.parse('$base/services'));

    var data = <GroomingService>[];

    try {
      for (var item in (jsonDecode(res.body) as List<dynamic>)) {
        data.add(GroomingService.fromJson(item as Map<String, dynamic>));
      }
    } catch (err) {
      LogHelper.log(err);
    }
    return data;
  }

  Future<bool> updateGrooming(Map<String, dynamic> updates) async {
    try {
      final res = await http.post(Uri.parse('$base/update'), body: updates);

      return true;
    } catch (err) {
      return false;
    }
  }

  Future<List<Attendant>> getGroomingAttendants() async {
    final res = await http.get(Uri.parse('$base/attendants'));

    var data = <Attendant>[];

    try {
      for (var item in (jsonDecode(res.body) as List<dynamic>)) {
        data.add(Attendant.fromJson(item as Map<String, dynamic>));
      }
    } catch (err) {
      LogHelper.log(err);
    }

    return data;
  }
}
