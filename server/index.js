const express = require('express');
const app = express();
const cors = require('cors');
const port = process.env.PORT || 3000;

// middleware
app.use(express.json());
app.use(express.urlencoded());
app.use(cors());

const { MongoClient, ServerApiVersion, ObjectId, Db } = require('mongodb');
const uri = "mongodb+srv://happypaws:CYOrYDER0X7SXzES@happypaws.w981o.mongodb.net/happypaws?retryWrites=true&w=majority";
const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: false, serverApi: ServerApiVersion.v1 })



client.connect(err => {
  if(err) return console.error(err);
  console.log("database connected");
});

function database() {
    return client.db("happypaws");
}

app.get('/', (req, res) => {
    res.send('Happy Paws Server is Running')
});

app.post('/grooming/add', async (req, res) => {
    try {
        let data = req.body;
        data['_createdAt'] = Date.now();
        
        let branch = await database().collection("branch").findOne({
            code: data.code
        });

        data['_p_branch'] = branch;

        let doc = await client.db("happypaws").collection("grooming").insertOne(data);
        res.send(doc);
    } catch (error) {
        res.error(error);
    }
});

app.post('/grooming/update', async (req, res) => {
    let updates = req.body;
    const _id = updates.id;
    delete updates.id;

    updates['_updatedAt'] = Date.now();    
    
    if (updates.attendant) {
        let attendant = await getAttendant(updates.attendant);
        if (attendant) updates['_o_attendant'] = attendant;
    }

    if (updates.service) {
        let service = await getService(updates.service);
        if (service) {
            updates['_o_service'] = service;
        }
    }

    try {
        let doc = await client.db("happypaws").collection("grooming").updateOne({
            _id: ObjectId(_id)
        }, {
            $set: updates
        });
    
        res.send(doc);
    } catch (error) {
        console.log(error);
        res.send(error);
    }
});

app.get('/grooming/list', async (req, res) => {
    const branch = req.query.branch;
    const dateFrom = parseInt(req.query.dateFrom);
    const dateTo = parseInt(req.query.dateTo);

    let query = {};
    if (branch) query['branch']=branch;

    if (dateFrom && dateTo) {
        query['_createdAt'] = {$gte:dateFrom, $lt:dateTo}
    }

    let groomings = await client.db("happypaws").collection("grooming").find(query).sort({
        _createdAt: -1
    }).toArray();

    res.send(groomings);
});

app.get('/grooming/services', async (req, res) => {
    
    let services = await (await client.db("happypaws").collection("service").find()).toArray();
    res.send(services);
});

app.get('/grooming/attendants', async (req, res) => {
        
    let attendants = await (await client.db("happypaws").collection("attendant").find()).toArray();
    
    res.send(attendants);
});

app.listen(port, () => {
    console.log(`server app listening on port ${port}`)
})


async function getAttendant(id) {
    return await database().collection("attendant").findOne({
        _id: ObjectId(id)
    });
}

async function getService(id) {
    return await database().collection("service").findOne({
        _id: ObjectId(id)
    });
}